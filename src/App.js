import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import MapboxGL from '@mapbox/react-native-mapbox-gl';
import { lineString as makeLineString } from '@turf/helpers';
import MapboxClient from 'mapbox';

const token = 'pk.eyJ1IjoieHRhYmJhcyIsImEiOiJjam9vYW55ejQxZmsyM3FxYTZ3OWdvdTN4In0.4etpj1EfBRrD5jPRG7Toxw'
const client = new MapboxClient(token);
MapboxGL.setAccessToken(token);

import Bubble from './Bubble';
import omnJSON from './countrygeo/omn_xgeo.json';
import allJSON from './countrygeo/allgeo.json';

const styles = MapboxGL.StyleSheet.create({
  countries: {
    fillAntialias: true,
    fillColor: 'blue',
    fillOutlineColor: 'black',
    fillOpacity: 0.84,
  },
  selectedCountry: {
    fillAntialias: true,
    fillColor: 'green',
    fillOpacity: 0.84,
  },
  omanCountry: {
    fillAntialias: true,
    fillColor: 'red',
    fillOpacity: 0.84,
  },
  route: {
    lineColor: 'black',
    lineWidth: 5,
    lineOpacity: 1,
  },
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.emptyState;
    this.onPress = this.onPress.bind(this);
  }

  get emptyState() {
    return { selectedGeoJSON: null, selectedCommunityDistrict: -1 };
  }

  async onPress(e) {
    const { screenPointX, screenPointY } = e.properties;

    const featureCollection = await this._map.queryRenderedFeaturesAtPoint(
      [screenPointX, screenPointY],
      null,
      ['worldFill'],
    );

    if (featureCollection.features.length) {
      const coords = featureCollection.features[0].geometry.coordinates;

      // NOTE: I tried using getDirections but this wasn't efficient to use
      // const res = await MapboxClient.getDirections(
      //   [
      //     { latitude: 55.89079837300008, longitude: 17.504543361000074 },
      //     // { latitude: coords[0][0][0], longitude: coords[0][0][1] },
      //   ],
      //   { profile: 'driving', geometry: 'polyline' },
      // );

      const res = await client.geocodeForward(featureCollection.features[0].properties.name, { types: 'country' });

      if(res.entity.features.length) {
        this.setState({
          selectedGeoJSON: featureCollection,
          selectedCommunityDistrict:
            featureCollection.features[0].properties.communityDistrict,
          route: makeLineString([
            [57.086424, 21.383637], // oman center
            res.entity.features[0].center
          ])
        });
      }


    } else {
      this.setState(this.emptyState);
    }
  }


  render () {
    return (
      <View style={{ flex: 1 }}>
        <MapboxGL.MapView
            zoomLevel={3}
            ref={(c) => (this._map = c)}
            onPress={this.onPress}
            centerCoordinate={[55.89079837300008, 17.504543361000074]}
            style={{ flex: 1 }}
            styleURL={MapboxGL.StyleURL.Light}>

            <MapboxGL.ShapeSource id="world" shape={allJSON}>
              <MapboxGL.FillLayer id="worldFill" style={styles.countries} />
            </MapboxGL.ShapeSource>

            <MapboxGL.ShapeSource id="oman" shape={omnJSON}>
              <MapboxGL.FillLayer id="omanFill" style={styles.omanCountry} />
            </MapboxGL.ShapeSource>

            {this.state.selectedGeoJSON ? (
              <MapboxGL.ShapeSource
                id="selectedCountry"
                shape={this.state.selectedGeoJSON}>
                <MapboxGL.FillLayer
                  id="selectedCountryFill"
                  style={styles.selectedCountry}
                />
              </MapboxGL.ShapeSource>
            ) : null}

            {this.state.selectedGeoJSON ? (
              <MapboxGL.ShapeSource id="routeSource" shape={this.state.route}>
                <MapboxGL.LineLayer
                  id="routeFill"
                  style={styles.route}
                />
              </MapboxGL.ShapeSource>
            ) : null}

          </MapboxGL.MapView>

          {this.state.selectedGeoJSON ? (
            <Bubble>
              <Text>{this.state.selectedGeoJSON.features[0].properties.name}</Text>
            </Bubble>
          ) : null}

      </View>
    )
  }
}

export default App;
